package com.ovelheirocode.pettrackerwebservice.dao;

import com.ovelheirocode.pettrackerwebservice.entity.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<Pet, Integer> {
}
