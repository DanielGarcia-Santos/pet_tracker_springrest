package com.ovelheirocode.pettrackerwebservice.dao;

import com.ovelheirocode.pettrackerwebservice.entity.PetType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetTypeRepository extends JpaRepository<PetType, Integer> {
}
