package com.ovelheirocode.pettrackerwebservice.dao;

import com.ovelheirocode.pettrackerwebservice.entity.Walk;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WalkRepository extends JpaRepository<Walk, Integer> {
}
