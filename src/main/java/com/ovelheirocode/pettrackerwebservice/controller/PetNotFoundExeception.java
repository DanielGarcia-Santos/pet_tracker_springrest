package com.ovelheirocode.pettrackerwebservice.controller;

public class PetNotFoundExeception extends RuntimeException {

    public PetNotFoundExeception(String message) {
        super(message);
    }

    public PetNotFoundExeception(String message, Throwable cause) {
        super(message, cause);
    }

    public PetNotFoundExeception(Throwable cause) {
        super(cause);
    }
}
