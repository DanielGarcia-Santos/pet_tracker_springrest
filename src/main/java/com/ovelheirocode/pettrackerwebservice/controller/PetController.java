package com.ovelheirocode.pettrackerwebservice.controller;

import com.ovelheirocode.pettrackerwebservice.entity.Pet;
import com.ovelheirocode.pettrackerwebservice.entity.PetType;
import com.ovelheirocode.pettrackerwebservice.entity.Walk;
import com.ovelheirocode.pettrackerwebservice.service.PetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PetController {

    @Autowired
    private PetService petService;

    @GetMapping("/pets")
    public List<Pet> findAllPets() {
        return petService.listPet();
    }

    @GetMapping("/pets/{petId}")
    public Pet findPet(@PathVariable int petId){
        Pet pet = petService.findPetById(petId);

        if(pet == null) {
            throw new PetNotFoundExeception("Pet id not found - " + petId);
        }

        return pet;
    }

    @PostMapping("/pets")
    public Pet addPet(@RequestBody Pet pet) {
        pet.setId(0);
        petService.savePet(pet);
        return pet;
    }

    @PutMapping("/pets")
    public Pet editPet(@RequestBody Pet pet) {
        petService.savePet(pet);
        return pet;
    }

    @DeleteMapping("/pets/{petId}")
    public String deletePet(@PathVariable int petId) {

        Pet tempPet = petService.findPetById(petId);

        if(tempPet == null) {
            throw new PetNotFoundExeception("Pet id not found - " + petId);
        }

        petService.deletePet(petId);

        return "Deleted pet id - " + petId;
    }

    @GetMapping("/walks")
    public List<Walk> findAllWalks() {
        return petService.listWalk();
    }

    @GetMapping("/walks/{walkId}")
    public Walk findWalk(@PathVariable int walkId){
        return petService.findWalkById(walkId);
    }

    @PostMapping("/walks")
    public Walk addWalk(@RequestBody Walk walk) {
        walk.setId(0);
        petService.saveWalk(walk);
        return walk;
    }

    @PutMapping("/walks")
    public Walk editWalk(@RequestBody Walk walk) {
        petService.saveWalk(walk);
        return walk;
    }

    @DeleteMapping("/walks/{walkId}")
    public String deleteWalk(@PathVariable int walkId) {

        Walk tempWalk = petService.findWalkById(walkId);

        if(tempWalk == null) {
            throw new PetNotFoundExeception("Walk id not found - " + walkId);
        }

        petService.deleteWalk(walkId);

        return "Deleted pet id - " + walkId;
    }

    @GetMapping("/petTypes")
    public List<PetType> findAllPetTypes() {
        return petService.listPetType();
    }

    @GetMapping("/petTypes/{petTypeId}")
    public PetType findPetType(@PathVariable int petTypeId){
        return petService.findPetTypeById(petTypeId);
    }

    @PostMapping("/petTypes")
    public PetType addPetType(@RequestBody PetType petType) {
        petType.setId(0);
        petService.savePetType(petType);
        return petType;
    }

    @PutMapping("/petTypes")
    public PetType editPetType(@RequestBody PetType petType) {
        petService.savePetType(petType);
        return petType;
    }

    @DeleteMapping("/petTypes/{petTypeId}")
    public String deletePetTypeId(@PathVariable int petTypeId) {

        PetType tempPetType = petService.findPetTypeById(petTypeId);

        if(tempPetType == null) {
            throw new PetNotFoundExeception("PetType id not found - " + petTypeId);
        }

        petService.deletePetType(petTypeId);

        return "Deleted pet id - " + petTypeId;
    }

}
