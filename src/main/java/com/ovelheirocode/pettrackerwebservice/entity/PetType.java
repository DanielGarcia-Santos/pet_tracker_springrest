package com.ovelheirocode.pettrackerwebservice.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "pet_type")
public class PetType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "pet_type_name")
    @NotNull(message = "is required")
    private String petTypeName;

    public PetType() {
    }

    public PetType(String petTypeName) {
        this.petTypeName = petTypeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPetTypeName() {
        return petTypeName;
    }

    public void setPetTypeName(String petTypeName) {
        this.petTypeName = petTypeName;
    }

    @Override
    public String toString() {
        return "PetType{" +
                "id=" + id +
                ", petTypeName='" + petTypeName + '\'' +
                '}';
    }
}
