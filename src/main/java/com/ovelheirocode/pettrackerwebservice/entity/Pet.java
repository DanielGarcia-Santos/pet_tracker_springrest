package com.ovelheirocode.pettrackerwebservice.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "pet")
public class Pet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "pet_name")
    private String petName;

    @OneToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name="id")
    private PetType petTypeId;

    @Column(name = "pet_sex")
    private String petSex;

    @Column(name = "pet_dob")
    private String petDob;

    @Column(name = "pet_weigth")
    private String petWeigth;

    @Column(name = "pet_pic_url")
    private String petPicUrl;


    public Pet() {
    }

    public Pet(String petName, PetType petTypeId) {
        this.petName = petName;
        this.petTypeId = petTypeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public PetType getPetTypeId() {
        return petTypeId;
    }

    public void setPetTypeId(PetType petTypeId) {
        this.petTypeId = petTypeId;
    }

    public String getPetSex() {
        return petSex;
    }

    public void setPetSex(String petSex) {
        this.petSex = petSex;
    }

    public String getPetDob() {
        return petDob;
    }

    public void setPetDob(String petDob) {
        this.petDob = petDob;
    }

    public String getPetWeigth() {
        return petWeigth;
    }

    public void setPetWeigth(String petWeigth) {
        this.petWeigth = petWeigth;
    }

    public String getPetPicUrl() {
        return petPicUrl;
    }

    public void setPetPicUrl(String petPicUrl) {
        this.petPicUrl = petPicUrl;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "id=" + id +
                ", petName='" + petName + '\'' +
                ", petTypeId=" + petTypeId +
                ", petSex='" + petSex + '\'' +
                ", petDob=" + petDob +
                ", petWeigth='" + petWeigth + '\'' +
                ", petPicUrl='" + petPicUrl + '\'' +
                '}';
    }
}
