package com.ovelheirocode.pettrackerwebservice.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "walk")
public class Walk {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id")
   private int id;

   @OneToMany(mappedBy = "id",
           cascade = {CascadeType.DETACH, CascadeType.MERGE,
                      CascadeType.PERSIST, CascadeType.REFRESH})
   private List<Pet> petIds;

   @Column(name = "walk_date")
   private String walkDate;

   @Column(name = "walk_name")
   private String walkName;

   @Column(name="walk_duration")
   private String walkDuration;

   @Column(name = "walk_distance")
   private String walkDistance;

   @Column(name = "walk_map_points")
   private String walkMapPoints;

   // empty constructor
   public Walk() {
   }

   // constructor to initialize only the not null fields
   public Walk(List<Pet> petIds) {
      this.petIds = petIds;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public List<Pet> getPetIds() {
      return petIds;
   }

   public void setPetIds(List<Pet> petIds) {
      this.petIds = petIds;
   }

   public String getWalkDate() {
      return walkDate;
   }

   public void setWalkDate(String walkDate) {
      this.walkDate = walkDate;
   }

   public String getWalkName() {
      return walkName;
   }

   public void setWalkName(String walkName) {
      this.walkName = walkName;
   }

   public String getWalkDuration() {
      return walkDuration;
   }

   public void setWalkDuration(String walkDuration) {
      this.walkDuration = walkDuration;
   }

   public String getWalkDistance() {
      return walkDistance;
   }

   public void setWalkDistance(String walkDistance) {
      this.walkDistance = walkDistance;
   }

   public String getWalkMapPoints() {
      return walkMapPoints;
   }

   public void setWalkMapPoints(String walkMapPoints) {
      this.walkMapPoints = walkMapPoints;
   }

   @Override
   public String toString() {
      return "Walk{" +
              "id=" + id +
              ", petIds=" + petIds +
              ", walkDate='" + walkDate + '\'' +
              ", walkName='" + walkName + '\'' +
              ", walkDuration='" + walkDuration + '\'' +
              ", walkDistance='" + walkDistance + '\'' +
              ", walkMapPoints='" + walkMapPoints + '\'' +
              '}';
   }
}
