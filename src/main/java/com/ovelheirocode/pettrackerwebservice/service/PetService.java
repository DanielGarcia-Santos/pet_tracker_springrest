package com.ovelheirocode.pettrackerwebservice.service;

import com.ovelheirocode.pettrackerwebservice.entity.Pet;
import com.ovelheirocode.pettrackerwebservice.entity.PetType;
import com.ovelheirocode.pettrackerwebservice.entity.Walk;

import java.util.List;

public interface PetService {

    // pet crud
    Pet savePet(Pet pet);
    Pet findPetById (int petId);
    List<Pet> listPet();
    void deletePet(int petId);

    // walk crud
    Walk saveWalk (Walk walk);
    Walk findWalkById (int walkId);
    List<Walk> listWalk();
    void deleteWalk(int walkId);

    // pet type crud
    PetType savePetType(PetType petType);
    PetType findPetTypeById (int petTypeId);
    List<PetType> listPetType();
    void deletePetType(int walkId);


}
