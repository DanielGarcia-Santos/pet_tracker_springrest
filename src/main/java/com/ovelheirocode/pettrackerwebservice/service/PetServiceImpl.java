package com.ovelheirocode.pettrackerwebservice.service;

import com.ovelheirocode.pettrackerwebservice.dao.PetRepository;
import com.ovelheirocode.pettrackerwebservice.dao.PetTypeRepository;
import com.ovelheirocode.pettrackerwebservice.dao.WalkRepository;
import com.ovelheirocode.pettrackerwebservice.entity.Pet;
import com.ovelheirocode.pettrackerwebservice.entity.PetType;
import com.ovelheirocode.pettrackerwebservice.entity.Walk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PetServiceImpl implements PetService {

    @Autowired
    private PetRepository petRepository;

    @Autowired
    private WalkRepository walkRepository;

    @Autowired
    private PetTypeRepository petTypeRepository;

    @Override
    public Pet savePet(Pet pet) {

        return petRepository.save(pet);
    }

    @Override
    public Pet findPetById(int petId) {

        //use java optionals
        //java 8 feature
        Optional<Pet> result = petRepository.findById(petId);

        Pet tempPet = null;

        if(result.isPresent()) {

            tempPet = result.get();

        } else {

            throw new RuntimeException("Did not find pet id - " + petId);
        }

        return tempPet;
    }

    @Override
    public List<Pet> listPet() {
        return petRepository.findAll();
    }

    @Override
    public void deletePet(int petId) {
        petRepository.deleteById(petId);
    }

    @Override
    public Walk saveWalk(Walk walk) {
        return walkRepository.save(walk);
    }

    @Override
    public Walk findWalkById(int walkId) {
        Optional<Walk> result = walkRepository.findById(walkId);

        Walk theWalk = null;

        if(result.isPresent()) {
            theWalk = result.get();
        } else {
            throw new RuntimeException("Did not find walk id - " + walkId);
        }
        return theWalk;
    }

    @Override
    public List<Walk> listWalk() {
        return walkRepository.findAll();
    }

    @Override
    public void deleteWalk(int walkId) {
        walkRepository.deleteById(walkId);
    }

    @Override
    public PetType savePetType(PetType petType) {
        return petTypeRepository.save(petType);
    }

    @Override
    public PetType findPetTypeById(int petTypeId) {
        Optional<PetType> result = petTypeRepository.findById(petTypeId);

        PetType thePetType = null;

        if(result.isPresent()) {
            thePetType = result.get();
        } else {
            throw new RuntimeException("Did not find pet type id - " + petTypeId);
        }
        return thePetType;
    }

    @Override
    public List<PetType> listPetType() {
        return petTypeRepository.findAll();
    }

    @Override
    public void deletePetType(int petTypeId) {
        petTypeRepository.deleteById(petTypeId);
    }
}
