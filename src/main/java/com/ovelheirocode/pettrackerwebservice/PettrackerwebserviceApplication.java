package com.ovelheirocode.pettrackerwebservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PettrackerwebserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PettrackerwebserviceApplication.class, args);
	}

}
